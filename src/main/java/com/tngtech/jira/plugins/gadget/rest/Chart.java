package com.tngtech.jira.plugins.gadget.rest;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Chart {
	@XmlElement
	public String image;
	@XmlElement
	public List<String> groupValues;
	@XmlElement
	public List<List<String>> data;
	@XmlElement
	public String projectOrFilterName;
	@XmlElement
	public String countIssues;
	@XmlElement
	public String groupBy;

	public Chart(String image, List<String> groupValues, List<List<String>> data, String projectOrFilterName,
			String countIssues, String groupBy) {
		this.image = image;
		this.groupValues = groupValues;
		this.data = data;
		this.projectOrFilterName = projectOrFilterName;
		this.countIssues = countIssues;
		this.groupBy = groupBy;
	}

	@SuppressWarnings("unused") public Chart() {}
}