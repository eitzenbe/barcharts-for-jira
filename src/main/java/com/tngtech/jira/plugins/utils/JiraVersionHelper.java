package com.tngtech.jira.plugins.utils;

//#global jirav=64

import org.jfree.util.Log;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.user.ApplicationUser;

public class JiraVersionHelper {

  public IssueType getIssueType(Issue is) {
    return 
//#if jirav==64 
    is.getIssueTypeObject();
//#else
    is.getIssueType();
//#endif
  }
  
  public boolean isEditable(Issue is) {
    IssueManager imgr = ComponentAccessor.getIssueManager();    
    //#if jirav==64
    User usr = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
    return imgr.isEditable(is, usr);    
    //#else
    ApplicationUser usr = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
    return imgr.isEditable(is, usr);    
    //#endif
  }

  public boolean checkIssuePermission(ProjectPermissionKey permkey, Issue is) {
    PermissionManager gpermgr = ComponentAccessor.getPermissionManager();
    //#if jirav==64
    ApplicationUser u64 = ComponentAccessor.getJiraAuthenticationContext().getUser();
    return gpermgr.hasPermission(permkey, is, u64);
    //#else
    ApplicationUser u71 = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
    return gpermgr.hasPermission(permkey, is, u71);
    //#endif
  }

  public boolean checkProjectPermission(ProjectPermissionKey permkey, Project p) {
    PermissionManager gpermgr = ComponentAccessor.getPermissionManager();
    //#if jirav==64
    ApplicationUser u64 = ComponentAccessor.getJiraAuthenticationContext().getUser();
    return gpermgr.hasPermission(permkey, p, u64);
    //#else
    ApplicationUser u71 = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
    return gpermgr.hasPermission(permkey, p, u71);
    //#endif
  }

  public Issue createIssue(Issue is) throws CreateException {
    IssueManager ismgr = ComponentAccessor.getIssueManager();
    //#if jirav==64
    User u64 = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
    return ismgr.createIssueObject(u64, is);
    //#else
    ApplicationUser u71 = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
    return ismgr.createIssueObject(u71, is);
    //#endif
  }
  
  public Issue updateIssue(MutableIssue is, boolean email) throws CreateException {
    IssueManager ismgr = ComponentAccessor.getIssueManager();
    //#if jirav==64
    User u64 = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
    return ismgr.updateIssue(u64, is, EventDispatchOption.ISSUE_UPDATED, email);
    //#else
    ApplicationUser u71 = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
    return ismgr.updateIssue(u71, is, EventDispatchOption.ISSUE_UPDATED, email);
    //#endif
  }

  public void setAssignee(MutableIssue is) throws CreateException {
    IssueManager ismgr = ComponentAccessor.getIssueManager();
    //#if jirav==64
    User usr = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
    is.setAssignee(usr);
    //#else
    ApplicationUser usr = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
    is.setAssignee(usr);
    //#endif
  }
}
