package com.tngtech.jira.plugins.utils;

import java.util.List;
import java.util.Set;

import javax.inject.Named;

//#if jirav==64
import com.atlassian.crowd.embedded.api.User;
//#else
import com.atlassian.jira.user.ApplicationUser;
//#endif
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.web.bean.I18nBean;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.query.Query;
import com.google.common.collect.ImmutableSet;
import com.tngtech.jira.plugins.utils.fields.CommonField;

@ExportAsService({ JiraUtils.class })
@Named("JiraUtils")
public class JiraUtils {
	private static final Set<String> VALID_CUSTOM_FIELD_KEYS = ImmutableSet.of(
			"com.atlassian.jira.plugin.system.customfieldtypes:select",
			"com.atlassian.jira.plugin.system.customfieldtypes:multicheckboxes",
			"com.atlassian.jira.plugin.system.customfieldtypes:datetime",
			"com.atlassian.jira.plugin.system.customfieldtypes:cascadingselect",
			"com.atlassian.jira.plugin.system.customfieldtypes:textfield",
			"com.atlassian.jira.plugin.system.customfieldtypes:multiuserpicker",
			"com.atlassian.jira.plugin.system.customfieldtypes:multigrouppicker",
			"com.atlassian.jira.plugin.system.customfieldtypes:userpicker",
			"com.atlassian.jira.plugin.system.customfieldtypes:datepicker",
			"com.atlassian.jira.plugin.system.customfieldtypes:version",
			"com.atlassian.jira.plugin.system.customfieldtypes:url",
			"com.atlassian.jira.plugin.system.customfieldtypes:textarea",
			"com.atlassian.jira.plugin.system.customfieldtypes:grouppicker",
			"com.atlassian.jirafisheyeplugin:hiddenjobswitch",
			"com.atlassian.jirafisheyeplugin:jobcheckbox",
			"com.atlassian.jira.plugin.system.customfieldtypes:multiselect",
			"com.atlassian.jira.plugin.system.customfieldtypes:radiobuttons",
			"com.atlassian.jira.plugin.system.customfieldtypes:project",
			"com.atlassian.jira.plugin.system.customfieldtypes:labels",
			"com.atlassian.jira.plugin.system.customfieldtypes:multiversion",
			"com.atlassian.jira.plugin.system.customfieldtypes:float",
			"et.seleniet.jitest.jitest-jira-plugin:ji-exec-state-cf-type",
			"et.seleniet.jitest.jitest-jira-plugin:ji-issue-picker-cf-type",
			"com.atlassian.jira.plugin.system.customfieldtypes:readonlyfield"
	);

	private final JiraAuthenticationContext authenticationContext;
	private final SearchService searchService;
	private final SearchRequestService searchRequestService;

  private static final JiraVersionHelper jvh = new JiraVersionHelper();

	public JiraUtils() {
		this.authenticationContext = ComponentAccessor.getJiraAuthenticationContext();
		this.searchService = ComponentAccessor.getComponentOfType(SearchService.class);
		this.searchRequestService = ComponentAccessor.getComponentOfType(SearchRequestService.class);
	}

//#if jirav==64
//#local UserPre=""
//#else
//#local UserPre="Application"
//#endif

	private /*$UserPre$*/User getApplicationUser() {
		return authenticationContext.getLoggedInUser();
	}

	public SearchRequest getSearchRequestForFilter(Long filterId) {
		JiraServiceContext jiraServiceContext = new JiraServiceContextImpl(getApplicationUser());
		return searchRequestService.getFilter(jiraServiceContext, filterId);
	}

	@SuppressWarnings("rawtypes")
	public SearchResults getSearchResults(Query query, PagerFilter pagerFilter) throws SearchException {
		return searchService.search(getApplicationUser(), query, pagerFilter);
	}

	public Project getProjectById(Long projectId) {
		ProjectManager projectManager = ComponentAccessor.getProjectManager();
		Project project = projectManager.getProjectObj(projectId);
		if (jvh.checkProjectPermission(ProjectPermissions.BROWSE_PROJECTS, project)) {
			return project;
		} else {
			return null;
		}
	}

	public CustomField getCustomFieldById(Long fieldId) {
		CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
		return customFieldManager.getCustomFieldObject(fieldId);
	}

	public CustomField getCustomFieldByName(String fieldName) {
		CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
		return customFieldManager.getCustomFieldObjectByName(fieldName);
	}

	public List<Option> getAllCustomFieldOptions() {
		OptionsManager manager = ComponentAccessor.getOptionsManager();
		return manager.getAllOptions();
	}

	public List<CustomField> getAllCustomFields() {
		CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
		return customFieldManager.getCustomFieldObjects();
	}

	public I18nBean geti18nBean() {
		return new I18nBean(getApplicationUser());
	}

	public String getTranslatedText(String key) {
		return authenticationContext.getI18nHelper().getText(key);
	}

	public boolean isValidType(String customFieldType) {
		return true; // TODO VALID_CUSTOM_FIELD_KEYS.contains(customFieldType);
	}

	public String getLabel(CommonField field) {
		return getTranslatedText(field.getKey());
	}
}
